USE master;  
GO  

--Delete the Razbitsky database if it exists.  
IF EXISTS(SELECT name from sys.databases WHERE name=N'Razbitsky')  
BEGIN  
    ALTER DATABASE [Razbitsky] set single_user with rollback immediate
END  

IF EXISTS(SELECT name from sys.databases WHERE name=N'Razbitsky')  
BEGIN  
    DROP DATABASE [Razbitsky]
END  

--Create a new database called TestData.  
CREATE DATABASE [Razbitsky]
GO

USE [Razbitsky] 
GO 

/*=== Создание таблиц ===*/

IF EXISTS(SELECT * FROM sys.schemas WHERE name = N'Погода') 
    DROP SCHEMA Погода
GO

CREATE SCHEMA Погода 
GO

/*Создаем таблицу "Гидрометеорологические_станции"*/

IF OBJECT_ID('[Razbitsky].Погода.Гидрометеорологические_станции', 'U') IS NOT NULL
  DROP TABLE  [Razbitsky].Погода.Гидрометеорологические_станции
GO

CREATE TABLE [Razbitsky].Погода.Гидрометеорологические_станции
(
	id int IDENTITY(1, 1) NOT NULL, 
	Название nvarchar(40) NULL, 
	Местоположение nvarchar(40) NULL, 
    CONSTRAINT PK_id PRIMARY KEY (id) 
)
GO

/*Создаем таблицу "Типы_измерений"*/

IF OBJECT_ID('[Razbitsky].Погода.Типы_измерений', 'U') IS NOT NULL
  DROP TABLE  [Razbitsky].Погода.Типы_измерений
GO

CREATE TABLE [Razbitsky].Погода.Типы_измерений
(
	id int IDENTITY(1, 1) NOT NULL , 
	Название nvarchar(40) NOT NULL
    CONSTRAINT PK_id PRIMARY KEY (id) 
)
GO


/*Создаем таблицу "Измерения"*/

IF OBJECT_ID('[Razbitsky].Погода.Измерения', 'U') IS NOT NULL
  DROP TABLE  [Razbitsky].Погода.Измерения
GO

CREATE TABLE [Razbitsky].Погода.Измерения
(
	id int IDENTITY(1, 1) NOT NULL, 
	Дата date NOT NULL, 
	id_станция int NOT NULL, 
	id_тип_измерения int NOT NULL, 
	Результат int NOT NULL, 
    CONSTRAINT PK_id PRIMARY KEY (id),
    CONSTRAINT FK_Измерения_id_станция FOREIGN KEY(id_станция) REFERENCES Гидрометеорологические_станции(id),
    CONSTRAINT FK_Измерения_id_тип_измерения FOREIGN KEY(id_тип_измерения) REFERENCES Типы_измерений(id)
)
GO

/*=== Заполнение таблиц ===*/

/* Заполняем таблицу "Гидрометеорологические_станции" */
 
INSERT INTO [Razbitsky].Погода.Гидрометеорологические_станции(Название, Местоположение)
  VALUES
(N'Станция 1',N'Москва')
, (N'Станция 2',N'Екатеринбург')
, (N'Станция 3',N'Камчатка')
, (N'Станция 4',N'Новая земля')
, (N'Станция 5',N'Москва')


/* Заполняем таблицу "Единицы_измерений" */

INSERT INTO [Razbitsky].Погода.Типы_измерений(Название)
  VALUES
(N'Температура')
, (N'Влажность')
, (N'Давление')

/* Заполняем таблицу "Измерения" */

INSERT INTO [Razbitsky].Погода.Измерения(Дата, id_станция, id_тип_измерения, Результат)
  VALUES
(N'2014-08-26', 1, 2, 87)
, (N'2014-08-26', 1, 3, 726)
, (N'2014-08-26', 1, 1, 21)
, (N'2014-08-26', 2, 2, 72)
, (N'2014-08-26', 2, 3, 731)
, (N'2014-08-26', 2, 1, 16)
, (N'2014-08-26', 3, 2, 81)
, (N'2014-08-26', 3, 3, 721)
, (N'2014-08-26', 3, 1, 9)
, (N'2014-08-26', 4, 2, 85)
, (N'2014-08-26', 4, 3, 733)
, (N'2014-08-26', 4, 1, -4)


SELECT гс.Название Станция, ти.Название Тип_измерения, 
    AVG(SELECT Результат 
        FROM Погода.Измерения ви 
        WHERE ви.Дата = и.Дата
            AND ви.id_станция = и.id_станция
            AND ви.id_тип_измерения = и.id_тип_измерения) Среднее
FROM Погода.Измерения и
WHERE и.Дата = 2014-08-26
LEFT JOIN Погода.Гидрометеорологические_станции гс ON гс.id = и.id_станция
LEFT JOIN Погода.Типы_измерений ти ON ти.id = и.id_тип_измерения
