
drop function get_beneficial

create function get_beneficial(@spent_minutes int)
	returns nvarchar(40)
	as begin
			declare @best_tariff_id int = (select top 1 t.id from tariff as t order by 
					dbo.formula(@spent_minutes, t.client_payment, t.minutes_available, t.payment_above)) 
			declare @best_tariff_name nvarchar(40) = (select top 1 t.tariff_name from tariff as t where t.id = @best_tariff_id)
			return @best_tariff_name
		end
		go

print dbo.get_beneficial(10)

drop function formula
-- ������� �������� ������������, ���� �� ����� ������ �����
create function formula(@minutes_spent int, @abonent_payment int, @available_by_abonent int,  @above_payment int)
	returns int
	as begin
		if (@minutes_spent <= @available_by_abonent)
			return @abonent_payment
		else
			begin
				declare @diff int
				set @diff = @minutes_spent - @available_by_abonent
				declare @total int
				set @total = @diff * @above_payment + @abonent_payment
				return @total
			end
		return -1
	end
	go


select u.minutes_used as '������������� �����', t.tariff_name as '��� ������', dbo.get_beneficial(u.minutes_used) as '����������� �����' from users as u
	join tariff as t on t.id = u.tariff_type