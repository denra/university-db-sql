
drop table Tariffs
create table Tariffs (
	ID int identity(1, 1) primary key,
	Name nvarchar(255),
	SubscriptionFee int,
	SubscriptionMinutes int	check (SubscriptionMinutes < 43201),
	ExtraMinuteFee int
)
go

insert into Tariffs values
			(N'��� ����������� �����', 0, 0, 2),
			(N'�������', 600, 400, 4),
			(N'�����������', 1200, 43200, 0)




if object_id('GetCost', 'FN') is not null
	drop function GetCost
go

create function GetCost(@Tariff int, @Minutes int) returns int	as 
begin
	declare @row table(ExtraMinuteFee int, SubscriptionMinutes int, SubscriptionFee int)	
	insert into @row select s.ExtraMinuteFee , s.SubscriptionMinutes, s.SubscriptionFee from Tariffs as s where s.id = @Tariff	
	declare @ExtraMinuteFee int = (select top(1) r.ExtraMinuteFee from @row as r)
	declare @SubscriptionMinutes int = (select top(1) r.SubscriptionMinutes from @row as r)
	declare @SubscriptionFee int = (select top(1) r.SubscriptionFee from @row as r)

	return @SubscriptionFee + @ExtraMinuteFee * (Case When @Minutes < @SubscriptionMinutes Then 0 Else @Minutes - @SubscriptionMinutes End)
end
go

if object_id('GetTarriffWithLowestCost', 'IF') is not null
	drop function GetTarriffWithLowestCost
go

create function GetTarriffWithLowestCost(@Minutes int) returns table as
	return (select top(1) Name, dbo.GetCost(ID, @Minutes) as Price from Tariffs order by Price asc)
go





if object_id('GetPointsFromTarrifs', 'TF') is not null
	drop function GetPointsFromTarrifs
go
drop function GetPointsFromTarrifs
create function GetPointsFromTarrifs(@s1 int, @s2 int) returns @table table(point int) as
begin
	declare @tariff1 table(ExtraMinuteFee int, SubscriptionMinutes int, SubscriptionFee int)
	declare @tariff2 table(ExtraMinuteFee int, SubscriptionMinutes int, SubscriptionFee int)
	insert into @tariff1 select t.ExtraMinuteFee, t.SubscriptionMinutes, t.SubscriptionFee from Tariffs as t where t.id = @s1
	insert into @tariff2 select t.ExtraMinuteFee, t.SubscriptionMinutes, t.SubscriptionFee from Tariffs as t where t.id = @s2
	declare @price1 int = (select top(1) ExtraMinuteFee from @tariff1)
	declare @price2 int = (select top(1) ExtraMinuteFee from @tariff2)
	declare @subscription1 int = (select top(1) SubscriptionFee from @tariff1)
	declare @subscription2 int = (select top(1) SubscriptionFee from @tariff2)
	declare @limit1 int = (select top(1) SubscriptionMinutes from @tariff1)
	declare @limit2 int = (select top(1) SubscriptionMinutes from @tariff2)

	insert into @table
	select (-@limit2*@price2+@subscription2 - @subscription1) / (-@price2) where @price2 > 0
	union
	select (-@limit1*@price1+@subscription1 - @subscription2) / (-@price1) where @price1 > 0
	union
	select (-@limit2*@price2+@subscription2 - (-@limit1*@price1+@subscription1)) / (@price1 - @price2) where @price1 != @price2
	union
	select 0

	return
end
go

 declare @other table(point int)
insert into @other select * from dbo.GetPointsFromTarrifs(1, 2)

select * from @other

if object_id('GetPointToCheck', 'TF') is not null
	drop function GetPointToCheck
go

create function GetPointToCheck() returns @table table(point int) as
begin
	declare @s1 int
	declare @s2 int
	declare foreach cursor for select s1.id, s2.id from Tariffs as s1 inner join Tariffs as s2 on s1.id < s2.id
	open foreach
	fetch next from foreach into @s1, @s2
	while (@@fetch_status = 0)
	begin
		insert into @table
			select * from dbo.GetPointsFromTarrifs(@s1, @s2)
			union select point - 1 from dbo.GetPointsFromTarrifs(@s1, @s2)
			union select point + 1 from dbo.GetPointsFromTarrifs(@s1, @s2)
		fetch next from foreach into @s1, @s2
	end
	close foreach
	deallocate foreach
	return
end
go




declare @points table(point int) 
insert into @points select distinct point from dbo.GetPointToCheck() where point >= 0 order by point asc
select * from @points

declare @table table(seg_start int, seg_end int, tariff_name nvarchar(255))
declare @prev int = 1
declare @p int
declare @stop int = 2147483647
declare @prev_lowest_tariff nvarchar(255)
declare @current_lowest_tariff nvarchar(255)
	
declare suspicious_points cursor for select point from @points union select @stop
open suspicious_points
fetch next from suspicious_points into @p
while (@@fetch_status = 0)
begin
	if @p = @stop
	begin
		insert into @table values (@prev, @p, @prev_lowest_tariff)
		break
	end

	set @current_lowest_tariff = (select name from dbo.GetTarriffWithLowestCost(@p))	
	if @current_lowest_tariff != @prev_lowest_tariff
	begin
		insert into @table values (@prev, @p - 1, @prev_lowest_tariff)
		set @prev = @p
	end
	set @prev_lowest_tariff = @current_lowest_tariff
	fetch next from suspicious_points into @p
end
close suspicious_points
deallocate suspicious_points

	
select * from @table

select * from dbo.GetTarriffWithLowestCost(500)