drop table tariff
 
create table tariff(
    id int primary key, --- id ������
    tariff_name nvarchar(100), -- ��� ������
    client_payment int, -- ����������� ����� ������� � ������ �� �����
    minutes_available int, -- ��������� ����� ����� �� ����������� �����
    payment_above int -- ����� �� ������ ������ ����� ������ �� ������� � ����������� ����� � ������
 
)

insert tariff(id, tariff_name, client_payment, minutes_available, payment_above) values
(1, '�����������', 1200, 43800, 0),
(2, '��� �.�', 0, 0, 2),
(3, '�������', 600, 400, 4) -- 600 ��� �.�����, �� ��� �������� 400 ���, ����� 400 ��� �� ������ ��� �� 4 �����


select tariff_name as '�����', client_payment as '����������� �����' from tariff



drop table users

create table users (
    id int identity(1, 1) primary key, -- id ������������
    tariff_type int, -- ��� ������, foreign key
    minutes_used int -- ������� ���������� ������ �� �����
    foreign key(tariff_type) references tariff(id)
    )


insert users(tariff_type, minutes_used) values
        (2, 100),
        (2, 400),
        (2, 250),
		(2, 300),
		(3, 100),
		(3, 150),
		(3, 200),
		(3, 250),
		(3, 300),
		(3, 400),
		(3, 450),
		(3, 500),
		(3, 550),
		(1, 100),
		(1, 300),
		(1, 400),
		(1, 550),
		(1, 600),
		(1, 650),
		(1, 700),
		(1, 900)

insert users(tariff_type, minutes_used) values
	(2, 600),
	(2, 550)


insert users(tariff_type, minutes_used) values
	(3, 500)

insert users(tariff_type, minutes_used) values
	(2, 500)