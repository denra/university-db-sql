drop table tariff
 
create table tariff(
    id int primary key, --- id ������
    tariff_name nvarchar(100), -- ��� ������
    client_payment int, -- ����������� ����� ������� � ������ �� �����
    minutes_available int, -- ��������� ����� ����� �� ����������� �����
    payment_above int -- ����� �� ������ ������ ����� ������ �� ������� � ����������� ����� � ������
 
)

insert tariff(id, tariff_name, client_payment, minutes_available, payment_above) values
(1, '��� �.�', 0, 0, 2),
(2, '�������', 600, 400, 4), -- 600 ��� �.�����, �� ��� �������� 400 ���, ����� 400 ��� �� ������ ��� �� 4 �����
(3, '�����������', 1200, 43800, 0)


drop function get_cost
create function get_cost(@tariff_id int, @minutes_used int) returns int
	begin
		
		declare @available_by_abonent int  = (select top(1) t.minutes_available from tariff as t where t.id = @tariff_id)
		declare @above_payment int = (select top(1) t.payment_above from tariff as t where t.id = @tariff_id)
		declare @abonent_payment int = (select top(1) t.client_payment from tariff as t where t.id = @tariff_id)
		
		if (@minutes_used <= @available_by_abonent)
			return @abonent_payment
		else
			begin
				declare @diff int
				set @diff = @minutes_used - @available_by_abonent
				declare @total int
				set @total = @diff * @above_payment + @abonent_payment
				return @total
			end
			return -1
		end
		go

drop function get_tariff_with_lowest_cost
create function get_tariff_with_lowest_cost(@minutes_used int) returns table
	as
		return (select top(1) tariff_name, dbo.get_cost(id, @minutes_used) as price from tariff order by price)


