IF OBJECT_ID('records', 'U') IS NOT NULL
  DROP TABLE  records;
 

create table records(
	record_id int identity(1, 1),
	post_id int,
	passing time,
	direction int,
	gov_number nvarchar(6),
	region_id nvarchar(3)

	primary key(record_id),
	foreign key(post_id) references  gibdd_posts(id),
	foreign key(region_id) references codes(num)

)

-- ������������ ����
insert into records(post_id, passing, direction, gov_number, region_id)
	values 
	(24, '12:11:12', 1, 'A360XY', '96')

-- �������������� ������
insert into records(post_id, passing, direction, gov_number, region_id)
	values 
	(1, '12:11:12', 1, 'A360XY', '144')