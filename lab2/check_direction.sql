if OBJECT_ID('check_dir', N'FN') IS NOT NULL
	DROP FUNCTION check_dir
GO

create function check_dir(@cur_dir int, @record_id int , @gov_number nvarchar(6), @now time) returns int
	as begin
		
		declare @dir int = (select top 1 direction from records 
			where record_id != @record_id and @gov_number = gov_number order by passing desc)
		if @dir is null return 1
		
		
		if (@cur_dir = @dir)
			return 0

			if (@cur_dir = 1)
				begin
					declare @last_time time = (select top 1 passing from records 
					where record_id != @record_id and @gov_number = gov_number 
						and direction = 0 and @now > passing)
					if @last_time is null
						return 0
					return 1
				end
			if (@cur_dir = 0)
				begin
					declare @last_t time = (select top 1 passing from records 
					where record_id != @record_id and @gov_number = gov_number 
						and direction = 1 and @now > passing)
					if @last_t is null
						return 0
					return 1
				end
		return 1
	end
	go

alter table records
	add constraint dir_corr check(dbo.check_dir(direction, record_id, gov_number, passing)=1)


-- ���������
insert into records(post_id, passing, direction, gov_number, region_id)
	values 
	(1, '10:11:12', 1, '�754KA', '96'),
	(1, '10:11:13', 0, '�754KA', '96');


-- ����������� � ����� ������ �������
insert into records(post_id, passing, direction, gov_number, region_id)
	values 
	(1, '10:10:17', 1, '�754KA', '96')

-- ����������� � ����� ������ �����������
insert into records(post_id, passing, direction, gov_number, region_id)
	values 
	(1, '10:11:17', 0, '�754KA', '96')

-- ��������� � ����� ������ �����������
insert into records(post_id, passing, direction, gov_number, region_id)
	values 
	(1, '10:11:17', 1, '�754KA', '96')


-- ����������� � ����� ������ �������
insert into records(post_id, passing, direction, gov_number, region_id)
	values 
	(0, '9:10:17', 0, '�754KA', '96')

select post_id as '����', Convert(nvarchar, passing,108) as '�����', IIF(direction = 1, '��', '���') as '� �����', 
	gov_number as '���.�����', region_id as '������' from records