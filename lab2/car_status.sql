-- ���������� ����:
-- �����, ��� ������� ����� ����, ������� ����� ������ � ������������������ � ������ �������


insert into records(post_id, passing, direction, gov_number, region_id)
	values 
	(1, '13:11:12', 1, 'E360XY', '174'),
	(2, '15:23:11', 0, 'E360XY', '174')

declare @transit table(gov_number nvarchar(6))

insert into @transit select distinct l.gov_number from records as l
	where 
		l.direction = 1 and l.region_id != 66
		and 
	exists (select * from records as r 
			where l.gov_number = r.gov_number and l.post_id != r.post_id and l.passing <r.passing and r.direction = 0);


select distinct t.gov_number as '����������', r.region_id as '������' from @transit as t 
	join records as r on t.gov_number = r.gov_number


--- ����������� ����:
-- ����, ������� ������� � ������� ����� ���� ����

declare @betwencity table(gov_number nvarchar(6))

insert into @betwencity select distinct b.gov_number from records as b
	where b.direction = 1 and b.region_id != 66
		and
			exists (select * from records as r 
				where r.gov_number = b.gov_number and r.post_id = b.post_id and b.passing < r.passing and r.direction = 0
			)

select distinct t.gov_number as '�����������', r.region_id as '������' from @betwencity as t 
	join records as r on t.gov_number = r.gov_number


-- ������� : ������� � ��������� + �� ������� �������
declare @local table(gov_number nvarchar(6))

insert into @local select distinct l.gov_number from records as l
	where l.direction = 0 and ( l.region_id = 66 or l.region_id = 96 or l.region_id = 196)
		and
			exists (select * from records as r
				where r.gov_number = l.gov_number and l.passing < r.passing and r.direction = 1
			)

select distinct t.gov_number as '�������', r.region_id as '������' from @local as t 
	join records as r on t.gov_number = r.gov_number



-- ������
declare @other table(gov_number nvarchar(6))

insert into @other select distinct l.gov_number from records as l
	where l.gov_number not in (select * from @transit) and
		  l.gov_number not in (select * from @betwencity) and
		  l.gov_number not in (select * from @local)