IF OBJECT_ID('records', 'U') IS NOT NULL
  DROP TABLE  records
GO

if OBJECT_ID('check_number', 'U') IS NOT NULL
	DROP TRIGGER check_number
GO

if OBJECT_ID('check_correct_direction', N'FN') IS NOT NULL
	DROP FUNCTION check_correct_direction
GO

 if OBJECT_ID('get_status', N'FN') IS NOT NULL
	DROP FUNCTION get_status
GO

IF OBJECT_ID('codes', 'U') IS NOT NULL
  DROP TABLE  codes;

IF OBJECT_ID('regions', 'U') IS NOT NULL
  DROP TABLE  regions;

 IF OBJECT_ID('gibdd_posts', 'U') IS NOT NULL
  DROP TABLE  gibdd_posts;