﻿IF OBJECT_ID('regions', 'U') IS NOT NULL
  DROP TABLE  regions;
 
 
  create table regions (
    id int,
    region_name nvarchar(100)
    constraint id primary key(id)
  )
 
  INSERT INTO regions
  VALUES
    (50, N'Ìîñêîâñêàÿ Îáëàñòü'),
    (66, N'Ñâåðäëîâñêàÿ Îáëàñòü'),
    (74, N'×åëÿáèíñêàÿ Îáëàñòü')


IF OBJECT_ID('codes', 'U') IS NOT NULL
  DROP TABLE  codes;
 
  create table codes (
     id int,
     num nvarchar(3),
	 primary key(num),
     foreign key(id) references regions(id)
  )
 
  INSERT INTO codes
  VALUES
	(50, 50),
	(50, 90),
	(50, 150),
    (50, 190),
	(50, 750),
	(66, 66),
    (66, 96),
	(66, 196),
    (74, 74),
    (74, 174)


 IF OBJECT_ID('gibdd_posts', 'U') IS NOT NULL
  DROP TABLE  gibdd_posts;

create table gibdd_posts(
	id int identity(1,1), -- id ïîñòà,
	post_name nvarchar(50), -- èìÿ ïîñòà,
	primary key(id), 
);

 INSERT INTO gibdd_posts(post_name)
  VALUES
    ('Ñèáèðñêèé Òðàêò'),
	('×åëÿáèíñêèé Òðàêò'),
	('Êîëüöîâî')