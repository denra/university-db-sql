if OBJECT_ID('check_number', 'U') IS NOT NULL
	DROP TRIGGER check_number
GO

create trigger check_number
	on records after insert, update
	as begin
		declare @incorrect_numbers int
		select @incorrect_numbers = Count(*) from records 
			where records.gov_number 
				not like '[������������ABEKMHOPCTYX][0-9][0-9][0-9][������������ABEKMHOPCTYX][������������ABEKMHOPCTYX]';
		
		if (@incorrect_numbers > 0)
			begin
				print '������������ ������������ ������!';
				rollback transaction;
			end
	end
	go

-- ���������� ������
insert into records(post_id, passing, direction, gov_number, region_id)
	values 
	(1, '12:11:12', 1, 'A360XY', '96'),
	(1, '12:11:13', 0, 'A360XY', '96'),
	(2, '12:11:15', 0, 'M222XX', '66');

-- ������������ �����
insert into records(post_id, passing, direction, gov_number, region_id)
	values 
	(1, '12:11:12', 0, '000000', '96');

-- ������������ �����
insert into records(post_id, passing, direction, gov_number, region_id)
	values 
	(1, '12:11:12', 0, '�555��', '96');

select post_id as '����', Convert(nvarchar, passing,108) as '�����', IIF(direction = 1, '��', '���') as '� �����', 
	gov_number as '���.�����', region_id as '������' from records